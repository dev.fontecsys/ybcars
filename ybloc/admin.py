from django.contrib import admin
from ybloc.models import *

from import_export.admin import ImportExportModelAdmin

# Register your models here.
class ClientAdmin(admin.ModelAdmin):
    list_display = ('nom','prenom','email')
    list_filter = ('nom','prenom',)
    ordering = ('nom', )
    search_fields = ('nom','prenom',)

admin.site.register(Client, ClientAdmin)

class PaysAdmin(admin.ModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Pays, PaysAdmin)

class VilleAdmin(admin.ModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Ville, VilleAdmin)

class AbonnementAdmin(admin.ModelAdmin):
    list_display = ('mode','prix')
    list_filter = ('mode','prix')
    ordering = ('mode', )
    search_fields = ('mode','prix')

admin.site.register(Abonnement, AbonnementAdmin)

class QuartierAdmin(ImportExportModelAdmin):
    list_display = ('libelle','ville')
    list_filter = ('ville','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Quartier, QuartierAdmin)

class NotificationAdmin(admin.ModelAdmin):
    list_display = ('titre','message')
    list_filter = ('titre','message')
    ordering = ('titre', )
    search_fields =  ('titre','message')

admin.site.register(Notification, NotificationAdmin)

class PermisAdmin(admin.ModelAdmin):
    list_display = ('numero','date_emission')
    list_filter = ('numero','date_emission')
    ordering = ('numero', )
    search_fields = ('numero','date_emission')

admin.site.register(Permis, PermisAdmin)

class CompteAdmin(admin.ModelAdmin):
    list_display = ('login','last')
    list_filter = ('login','last')
    ordering = ('login', )
    search_fields = ('login','last')

admin.site.register(Compte, CompteAdmin)

class MarqueAdmin(ImportExportModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Marque, MarqueAdmin)

class ModeleAdmin(ImportExportModelAdmin):
    list_display = ('libelle','id')
    list_filter = ('libelle','id')
    ordering = ('libelle', )
    search_fields = ('libelle','id')

admin.site.register(Modele, ModeleAdmin)

class TypeVoitureAdmin(ImportExportModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(TypeVoiture, TypeVoitureAdmin)

class VehiculeAdmin(admin.ModelAdmin):
    list_display = ('description','status','created_at')
    list_filter = ('description','created_at')
    ordering = ('description', )
    search_fields = ('description','created_at')

admin.site.register(Vehicule, VehiculeAdmin)

class OptionAdmin(ImportExportModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Option, OptionAdmin)

class OptionVehiculeAdmin(admin.ModelAdmin):
    list_display = ('voiture','option', 'prix')
    list_filter = ('voiture','created_at')
    ordering = ('voiture', )
    search_fields = ('voiture','created_at')

admin.site.register(OptionVehicule, OptionVehiculeAdmin)

class OptionLocationAdmin(admin.ModelAdmin):
    list_display = ('location','created_at')
    list_filter = ('location','created_at')
    ordering = ('location', )
    search_fields = ('location','created_at')

admin.site.register(OptionLocation, OptionLocationAdmin)


class LocationAdmin(admin.ModelAdmin):
    list_display = ('client','voiture')
    list_filter = ('client','voiture')
    ordering = ('client', )
    search_fields = ('client','voiture')

admin.site.register(Location, LocationAdmin)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('titre','description')
    list_filter = ('titre','description')
    ordering = ('titre', )
    search_fields = ('titre','Description')

admin.site.register(Article, ArticleAdmin)

class CouleurAdmin(ImportExportModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Couleur, CouleurAdmin)

class PorteAdmin(ImportExportModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Porte, PorteAdmin)


class ProcurationAdmin(ImportExportModelAdmin):
    list_display = ('nom','prenom')
    list_filter = ('nom','prenom')
    ordering = ('nom', )
    search_fields = ('nom','prenom')

admin.site.register(Procuration, ProcurationAdmin)



class PlaceAdmin(ImportExportModelAdmin):
    list_display = ('libelle','created_at')
    list_filter = ('libelle','created_at')
    ordering = ('libelle', )
    search_fields = ('libelle','created_at')

admin.site.register(Place, PlaceAdmin)

class DocumentAdmin(ImportExportModelAdmin):
    list_display = ('date_debut','date_fin','reference', 'doc', 'type_doc')
    list_filter = ('date_debut','date_fin')
    ordering = ('date_debut', )
    search_fields = ('date_debut','date_fin')

admin.site.register(DocumentVehicule, DocumentAdmin)