# Generated by Django 3.0.2 on 2020-04-26 10:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import ybloc.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Abonnement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mode', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('prix', models.FloatField(blank=True, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(blank=True, max_length=50, null=True)),
                ('prenom', models.CharField(blank=True, max_length=50, null=True)),
                ('date_naissance', models.DateField(blank=True, null=True)),
                ('telephone', models.CharField(blank=True, max_length=30, null=True)),
                ('email', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('adresse', models.CharField(blank=True, max_length=100, null=True)),
                ('bp', models.CharField(blank=True, max_length=100, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('statut', models.CharField(blank=True, choices=[('Non_salarie', 'Non salarier'), ('Salarie', 'Salarier')], max_length=100, null=True, verbose_name='statut salarial')),
                ('titre', models.CharField(blank=True, choices=[('M', 'Monsieur'), ('MLLE', 'Mademoisele'), ('MME', 'Madame')], max_length=100, null=True, verbose_name='Titre')),
                ('doc', models.FileField(blank=True, null=True, upload_to=ybloc.models.path_and_rename_client)),
                ('entite', models.CharField(blank=True, max_length=100, null=True, verbose_name='Entité')),
                ('type', models.CharField(blank=True, choices=[('P', 'Propretaire'), ('L', 'Locataire')], max_length=1, null=True, verbose_name='type client')),
            ],
        ),
        migrations.CreateModel(
            name='Compte',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login', models.CharField(blank=True, max_length=30, null=True)),
                ('password', models.TextField()),
                ('vernum', models.CharField(blank=True, max_length=30, null=True)),
                ('vermail', models.CharField(blank=True, max_length=30, null=True)),
                ('verdoc', models.CharField(blank=True, max_length=30, null=True)),
                ('last', models.DateTimeField(blank=True, null=True)),
                ('profile_image', models.ImageField(blank=True, default='../static/images/default_avatar.png', null=True, upload_to='images/users/')),
            ],
        ),
        migrations.CreateModel(
            name='Couleur',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('avec_chauffeur', models.IntegerField(blank=True, null=True)),
                ('client_est_chauffeur', models.IntegerField(blank=True, null=True)),
                ('livraison_effective', models.IntegerField(blank=True, null=True)),
                ('long_duree', models.IntegerField(blank=True, null=True)),
                ('niveau_carburant', models.CharField(blank=True, max_length=5, null=True)),
                ('date_dbt', models.DateTimeField(blank=True, null=True)),
                ('date_fin', models.DateTimeField(blank=True, null=True)),
                ('tarif_journalier', models.FloatField(blank=True, null=True)),
                ('nbr_jour', models.FloatField(blank=True, null=True)),
                ('montant', models.FloatField(blank=True, null=True)),
                ('statut', models.CharField(blank=True, max_length=100, null=True)),
                ('lavage_carburant', models.FloatField(blank=True, null=True)),
                ('frais_livraison', models.FloatField(blank=True, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('retour', models.DateTimeField(blank=True, null=True)),
                ('comment', models.CharField(blank=True, max_length=100, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Client')),
            ],
        ),
        migrations.CreateModel(
            name='Marque',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(blank=True, max_length=250, null=True)),
                ('email', models.CharField(blank=True, max_length=250, null=True)),
                ('sujet', models.CharField(blank=True, max_length=250, null=True)),
                ('message', models.CharField(blank=True, max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Modele',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('marque', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Marque')),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(blank=True, max_length=250, null=True)),
                ('message', models.CharField(blank=True, max_length=250, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Option',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(max_length=191, unique=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('type', models.CharField(blank=True, choices=[('paie', 'paie'), ('free', 'free')], max_length=100, null=True, verbose_name='type option')),
            ],
        ),
        migrations.CreateModel(
            name='Pays',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Pays',
            },
        ),
        migrations.CreateModel(
            name='Permis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.CharField(max_length=191, unique=True)),
                ('fichier', models.FileField(blank=True, null=True, upload_to='permis/')),
                ('date_emission', models.DateField(blank=True, null=True)),
                ('type', models.CharField(choices=[('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D')], max_length=100, verbose_name='Type')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Permis',
            },
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Porte',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Procuration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('civilite', models.CharField(blank=True, choices=[('M', 'Monsieur'), ('MLLE', 'Mademoisele'), ('MME', 'Madame')], max_length=100, null=True, verbose_name='Titre')),
                ('nom', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('prenom', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('email', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('tel', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('date_naiss', models.DateTimeField(blank=True, null=True)),
                ('fichier', models.FileField(blank=True, null=True, upload_to='procuration/')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Quartier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Transmission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TypeVoiture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('modele', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Modele')),
            ],
        ),
        migrations.CreateModel(
            name='Usage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ville',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('libelle', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('pays', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Pays')),
            ],
        ),
        migrations.CreateModel(
            name='Vehicule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(blank=True, max_length=191, null=True)),
                ('prix', models.FloatField(blank=True, null=True)),
                ('prixKm', models.FloatField(blank=True, null=True)),
                ('kilometrage', models.FloatField(blank=True, null=True)),
                ('immatriculation', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('nr_chassis', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('date_mise_en_circulation', models.DateField(blank=True, null=True)),
                ('annee_mise_en_circulation', models.IntegerField(choices=[(2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020)], default=2020, verbose_name='année')),
                ('carburant', models.CharField(blank=True, choices=[('essence', 'essence'), ('Diesel', 'Diesel'), ('Hybride', 'Hybride'), ('Électrique', 'Électrique'), ('None', 'Aucun')], max_length=100, null=True, verbose_name='Carburant')),
                ('transmission', models.CharField(blank=True, choices=[('Automatique', 'Automatique'), ('Manuelle', 'Manuelle'), ('None', 'Aucun')], max_length=100, null=True, verbose_name='transmission')),
                ('proprietaire', models.CharField(blank=True, choices=[('oui', 'oui'), ('non', 'non'), ('None', 'Aucun')], max_length=100, null=True, verbose_name='proprietaire')),
                ('chauffeur', models.CharField(blank=True, choices=[('oui', 'oui'), ('non', 'non'), ('None', 'Aucun')], max_length=100, null=True, verbose_name='chauffeur')),
                ('mode_paie', models.CharField(blank=True, choices=[('abonnement', 'abonnement'), ('location', 'location'), ('None', 'Aucun')], max_length=100, null=True, verbose_name='Mode de paiement')),
                ('status', models.CharField(blank=True, choices=[('En aprobation', 'En aprobation'), ('Desactivé', 'desactivé'), ('Publié', 'Publié'), ('En location', 'En location'), ('None', 'Aucun')], max_length=100, null=True, verbose_name='status')),
                ('feature', models.FileField(blank=True, null=True, upload_to='voiture/')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('date_debut_location', models.DateTimeField(blank=True, null=True)),
                ('date_fin_location', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Client')),
                ('couleur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Couleur')),
                ('marque', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Marque')),
                ('modele', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Modele')),
                ('nbr_place', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Place')),
                ('nbr_porte', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Porte')),
                ('procuration', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Procuration')),
                ('quartier', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Quartier')),
                ('type_abonnement', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Abonnement')),
                ('type_vehicule', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.TypeVoiture')),
                ('usage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Usage')),
                ('ville', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Ville')),
            ],
        ),
        migrations.AddField(
            model_name='quartier',
            name='ville',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Ville'),
        ),
        migrations.AddField(
            model_name='procuration',
            name='quartier',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Quartier'),
        ),
        migrations.CreateModel(
            name='OptionVehicule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('prix', models.BigIntegerField(blank=True, null=True)),
                ('type', models.CharField(blank=True, choices=[('paie', 'paie'), ('free', 'free'), ('None', 'Aucun')], max_length=100, null=True, verbose_name='type option')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('option', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Option')),
                ('voiture', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Vehicule')),
            ],
        ),
        migrations.CreateModel(
            name='OptionLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('location', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Location')),
                ('option', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.OptionVehicule')),
            ],
        ),
        migrations.CreateModel(
            name='NotifUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Compte')),
                ('notification', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Notification')),
            ],
        ),
        migrations.AddField(
            model_name='location',
            name='voiture',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Vehicule'),
        ),
        migrations.CreateModel(
            name='GalleryVehicule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fichier', models.FileField(blank=True, null=True, upload_to='gallery/')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('voiture', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Vehicule')),
            ],
        ),
        migrations.CreateModel(
            name='Favoris',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Client')),
                ('voiture', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Vehicule')),
            ],
        ),
        migrations.CreateModel(
            name='DocumentVehicule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('doc', models.FileField(blank=True, null=True, upload_to=ybloc.models.path_and_rename)),
                ('date_debut', models.DateTimeField(blank=True, null=True)),
                ('date_fin', models.DateTimeField(blank=True, null=True)),
                ('reference', models.CharField(blank=True, max_length=191, null=True, unique=True)),
                ('type_doc', models.CharField(blank=True, choices=[('carte', 'carte'), ('assurance', 'assurance'), ('visite', 'visite')], max_length=100, null=True, verbose_name='type document')),
                ('voiture', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Vehicule')),
            ],
        ),
        migrations.CreateModel(
            name='Commentaire',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.CharField(blank=True, max_length=250, null=True)),
                ('etat', models.CharField(blank=True, max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Client')),
                ('voiture', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Vehicule')),
            ],
        ),
        migrations.AddField(
            model_name='client',
            name='compte',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Compte'),
        ),
        migrations.AddField(
            model_name='client',
            name='permis',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Permis'),
        ),
        migrations.AddField(
            model_name='client',
            name='quartier',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ybloc.Quartier'),
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(blank=True, max_length=200, null=True)),
                ('description', models.CharField(blank=True, max_length=200, null=True)),
                ('texte', models.TextField(blank=True, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='blog/')),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('auteur', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
