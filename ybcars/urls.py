"""ybcars URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from ybloc import views
from django.conf import settings 
from django.conf.urls.static import static # new

urlpatterns = [
    path('accueil/', views.home, name='accueil'),
    path('testerror/', views.testerror, name='testerror'),
    path('', views.home, name=''),
    path('blog/', views.blog, name='blog'),
    path('contact/', views.contact, name='contact'),
    path('apropos/', views.about, name='apropos'),
    path('faqs/', views.faqs, name='faqs'),
    path('inscription/', views.register, name='inscription'),
    path('ajout_user/',  views.addUser, name='ajout_user'),
    path('connexion/',  views.login, name='connexion'),
    path('profile/',  views.compte, name='compte'),
    path('majcompte/',  views.updateCompte, name='majcompte'),
    path('majPwd/',  views.UpdatePassword, name='majPwd'),
    path('ajoutForm/',  views.postForm, name='ajoutForm'),
    path('recherche/',  views.carList, name='recherche'),
    path('ajoutCar/',  views.postAdd, name='ajoutCar'),
    path('testJs/',  views.testJs, name='testJs'),
    path('modifCar/',  views.updateAdd, name='modifCar'),
    path('AfficheRent/',  views.showRent, name='AfficheRent'),
    path('saveRent/',  views.saveRent, name='saveRent'),
    path('compte/',  views.afficheCompte, name='compte'),
    re_path(r'^userPorfile/(?P<id>\d+)',  views.afficheRentByCompte, name='userPorfile'),
    path('verifyMail/',  views.verifyMail, name='verifyMail'),
    path('mailContact/',  views.SendMail, name='mailContact'),
    path('savenotif/',  views.saveNotif, name='savenotif'),
    path('changeStat/', views.carChangeStat, name='changeStat'),
    path('valideajoutvehicule/', views.valideajoutvehicule, name='validationvehicule'),
    path('comment/', views.commentCar, name='comment'),
    path('retour/', views.retourCar, name='retour'),
    path('resultat/', views.resultCar, name='resultat'),
    path('resultatHome/', views.resultHome, name='resultatHome'),
    path('planUpdate/', views.updatePlan, name='planUpdate'),

    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    path('ajoutForm/uploads/',  views.postForm, name='uploads'),
    path('ajout-vehicule/',  views.ajoutvehicule, name='ajout-vehicule'),
    re_path(r'^compte/(?P<id>\d+)',  views.afficheCompte, name='affiche_compte'),
    re_path(r'^testimmat/$', views.TestImmat, name='TestImmat'),
    re_path(r'^voiture/(?P<id>\d+)',  views.myCars, name='voiture'),
    re_path(r'^location/(?P<id>\d+)',  views.myRent, name='location'),
    re_path(r'^like/(?P<id>\d+)',  views.like, name='like'),
    re_path(r'^unlike/(?P<id>\d+)',  views.unLike, name='unlike'),
    re_path(r'^favourite/(?P<id>\d+)',  views.myLike, name='favoris'),
    re_path(r'^messages/(?P<id>\d+)',  views.myComment, name='message'),
    re_path(r'^UserAcces/(?P<id>\d+)',  views.userCompte, name='compte_acces'),
    re_path(r'^deconnexion/(?P<id>\d+)',  views.logout, name='deconnexion'),
    re_path(r'^majphoto/(?P<id>\d+)',  views.updateProfileImg, name='majphoto'),
    re_path(r'^carDetail/(?P<id>\d+)/',  views.carDetail, name='detailVoiture'),
    re_path(r'^articleDetail/(?P<id>\d+)/',  views.postDetail, name='articleDetail'),
    re_path(r'^editCar/(?P<id>\d+)',  views.postForm, name='editCar'),
    re_path(r'^deleteCar/(?P<id>\d+)',  views.delCar, name='deleteCar'),
    re_path(r'^changeStat/$',  views.carChangeStat, name='changeStat'),
    re_path(r'^deletGal/$',  views.carDeleteGal, name='deletGal'),
    re_path(r'^ajax_load_quartier/$',  views.load_quartier, name='ajax_load_quartier'),
    re_path(r'^ajax_load_modele/$',  views.load_modele, name='ajax_load_modele'),
    re_path(r'^ajax_test_immat/$',  views.TestImmat, name='ajax_test_immat'),
    re_path(r'^ajax_load_type/$',  views.load_type, name='ajax_load_type'),
    re_path(r'^validation-compte/(?P<id>\d+)', views.validationCompte, name='validation'),
    re_path(r'^validation-email/(?P<id>\d+)', views.validationMail, name='validation-email'),
    #re_path(r'^changeStat/(?P<id>\d+)/(?P<st>\d+)',  views.carChangeStat, name='changeStat'),
    path('admin/', admin.site.urls),
    #path("", include("social_django.urls", namespace="social")),
    #path('select2/', include('django_select2.urls')),
    #path('ajax/load-quartier/', views.load_quartier, name='ajax_load_quartier'),  # <-- this one here


]
if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
