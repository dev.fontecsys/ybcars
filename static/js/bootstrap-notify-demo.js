$.notify({
	// options
	title: '<strong>Génial ! </strong>',
	message: 'Inscription reussie, veuillez vous connecter',
	icon: 'icon-ok'

},{
	// settings
	type: 'success',
	placement: {
    		from: "top",
    		align: "center"
    	},
    animate: {
    		enter: 'animated bounceInDown',
    		exit: 'animated bounceOutUp'
    	}
});