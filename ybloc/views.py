from django.shortcuts import redirect, render
from ybloc import models 
from ybloc.models import *
from passlib.hash import django_pbkdf2_sha256 as handler# Create your views here.
from passlib.apps import custom_app_context as pwd_context
from datetime import datetime
#import datetime
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt,csrf_protect #Add this
from django.http import HttpResponse
from django.http import JsonResponse
import json
from django.utils.html import escape
from django.utils.datastructures import MultiValueDictKeyError


from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.db.models import Sum
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import *
from .utils_function import *

#Affiche la page d'accueil
def home(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
    vehiculeList = Vehicule.objects.all().order_by('created_at')
    nombre= vehiculeList.count()
    PaysList = Pays.objects.all()
    locationList = Location.objects.all()
    articleList= Article.objects.all()
    userList = Compte.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    MarqueList = Marque.objects.all()
    ModeleList = Modele.objects.all()
    TypeVoitureList = TypeVoiture.objects.all()
    optionList = Option.objects.all()
    favorisList = Favoris.objects.filter(client_id=client.id)
    YEAR_CHOICES = []
    for r in range(2005, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r))
    form_Voiture = VehiculeForm()
    return render(request, 'accueil.html', locals())

def testerror(request):
    return render(request, 'testerror.html', locals())

#Affiche le formulaire d'inscription
def register(request):
	return render(request, 'inscription.html', locals())

#mettre a jour les information du compte
def updateCompte(request):
    if request.method == 'POST':
        cltupdate = Client.objects.get(email=request.POST['email'])
        cltupdate.nom = request.POST['nom']
        cltupdate.prenom = request.POST['prenom']
        if request.POST['date'] != "jj/mm/aaa" and request.POST['date'] != "":
            cltupdate.date_naissance = request.POST['date']
        if request.POST['quartier'] != "" and request.POST['quartier'] != 'None': 
            cltupdate.quartier = Quartier.objects.get(id=request.POST['quartier'])
        if request.POST['phone'] != 'None':
            cltupdate.telephone = request.POST['phone']

        cltupdate.statut = request.POST['status']
        cltupdate.titre = request.POST['titre']
        doc= None
        if request.POST['status'] == "salarie":
            doc = request.POST['AttEmploi']
            cltupdate.entite = request.POST['ets']
            try:
                cltupdate.doc = request.FILES['imgEmp']
            except MultiValueDictKeyError:
                pass
        elif request.POST['status'] == "Etudiant":
            doc = request.POST['AttEcole']
            cltupdate.entite = request.POST['etab']
            try:
                cltupdate.doc = request.FILES['imgEcole']
            except MultiValueDictKeyError:
                pass
        if cltupdate.doc != doc and doc !=None: 
            fs = FileSystemStorage()
            name = fs.save(cltupdate.doc.name, cltupdate.doc)
        try:
            permis = Permis.objects.get(id=cltupdate.permis_id)
            permis.numero=request.POST['numPermis']
            permis.type=request.POST['typePermis']
        except Permis.DoesNotExist :
            permis = Permis()
            permis.numero=request.POST['numPermis']
            permis.type=request.POST['typePermis']
        #newdoc = Permis(fichier=request.FILES.get('myfile'))
        #newdoc.save()
        if permis.fichier != request.POST['fileUploaded'] and request.POST['fileUploaded'] != "" and request.POST['fileUploaded'] != 'None'  : 
            permis.fichier = request.FILES['imgInp']
            fs = FileSystemStorage()
            name = fs.save(permis.fichier.name, permis.fichier)
        permis.save()
        #uploaded_file_url = fs.url(filename)
        
        cltupdate.permis = permis
        cltupdate.save()
        return redirect('compte')
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
        ctx = statUser(request)
        ctx['cmpt'] = cmpt
        ctx['client'] =  client
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
        
    return render(request, 'compte.html', ctx)

#Affichage du compte
def afficheCompte(request, id=1):
    PaysList = Pays.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    doc_emp=""
    doc_etd = ""

    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        try:
            client = Client.objects.get(email= cmpt.login)
            if client.statut =="salarie":
                doc_emp = client.doc
            elif client.statut == "etudiant":
                doc_etd = client.doc
            notification = NotifUser.objects.filter(client_id=request.session.get('user_id'))
            testpro = TestProfil(client)
        except client.DoesNotExist:
            cmpt = Compte()
            client = Client()
            return redirect('connexion')

        listNotif = Notification.objects.all()
        for  notif in listNotif:
            for  notUser in notification:
                if notif.id == notUser.notification.id:
                    listNotif = listNotif.exclude(pk=notif.id)
                else:
                    pass      
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')

    try:
        plan = PaiementClient.objects.get(client_id=request.session.get('user_id'))
    except PaiementClient.DoesNotExist:
        plan = PaiementClient()
    
    MODE = []
    MODE.append(("Virement bancaire"))
    MODE.append(("Airtel money"))
    MODE.append(("Visa"))
    ctx = statUser(request)
    ctx['PaysList'] = PaysList
    ctx['VilleList'] =  VilleList
    ctx['QuartierList'] =  QuartierList
    ctx['notification'] =  notification
    ctx['listNotif'] =  listNotif
    ctx['doc_emp'] = doc_emp
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['doc_etd'] = doc_etd
    ctx['mode'] = MODE
    ctx['plan'] = plan
    ctx['Testprof']= int(testpro)
    return render(request, 'compte.html', ctx)

#Affichage du compte
def afficheRentByCompte(request, id=1):
    PaysList = Pays.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    doc_emp=""
    doc_etd = ""

    try:
        cmpt = Compte.objects.get(id=id) 
        client = Client.objects.get(email= cmpt.login)
        if client.statut =="salarie":
            doc_emp = client.doc
        elif client.statut == "etudiant":
            doc_etd = client.doc
        notification = NotifUser.objects.filter(client_id=request.session.get('user_id'))
        listNotif = Notification.objects.all()
        for  notif in listNotif:
            for  notUser in notification:
                if notif.id == notUser.notification.id:
                    listNotif = listNotif.exclude(pk=notif.id)
                else:
                    pass      
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')

    try:
        plan = PaiementClient.objects.get(client_id=request.session.get('user_id'))
    except PaiementClient.DoesNotExist:
        plan = PaiementClient()

    vide = False
    try:
        cartList = Vehicule.objects.filter(client=client.id)
        nombre= cartList.count()
    except Vehicule.DoesNotExist:
        vide = True

    MODE = []
    MODE.append(("Virement bancaire"))
    MODE.append(("Airtel money"))
    MODE.append(("Visa"))
    ctx = statUser(request)
    ctx['PaysList'] = PaysList
    ctx['VilleList'] =  VilleList
    ctx['cartList'] =  cartList
    ctx['QuartierList'] =  QuartierList
    ctx['notification'] =  notification
    ctx['listNotif'] =  listNotif
    ctx['doc_emp'] = doc_emp
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['doc_etd'] = doc_etd
    ctx['mode'] = MODE
    ctx['plan'] = plan
    return render(request, 'userProfil.html', ctx)

#Affiche le formulaire d'authentification
def login(request):


	return render(request, 'connexion.html', locals())

#mise a jour du plan de paiement
def updatePlan(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
    if request.method == 'POST':
        try:
            plan = PaiementClient.objects.get(client_id=request.session.get('user_id'))
            plan.mode = request.POST['plan']
            plan.reference = request.POST['cord']
            plan.client = client
            plan.save()
            return redirect('compte')
        except PaiementClient.DoesNotExist:
            plan = PaiementClient()
            plan.mode = request.POST['plan']
            plan.reference = request.POST['cord']
            plan.client = client
            plan.save()
            return redirect('compte')

    return render(request, 'compte.html', locals())

#connexion d'un utilisateur
def compte(request):
    TestEmail = 0
    if request.method == 'POST': 
        if request.POST['login'] != "" and request.POST['password'] != "":
            login = request.POST['login']
            passwd= request.POST['password']
            try:
                usr = Compte.objects.get(login=login)
                chk = handler.verify(passwd, usr.password)
                print("Ancien:"+usr.password)
                if chk==True:
                    conexOK = True
                    if usr.vermail == '1':
                        TestEmail = 1
                        request.session['user_id'] = usr.id
                        usr.last = datetime.datetime.now()
                        usr.save()
                    else:
                        TestEmail = 0
                else :
                    passError=True
            except Compte.DoesNotExist:
                personne = True
                
        else:

            erreur = True
            
    else:
        pass
        #usr =usr
    return render(request, 'connexion.html', locals())
	

#ajout d'un utilisateur
def addUser(request):
    if request.method == 'POST':
        if Client.objects.filter(email=request.POST['mail']).exists():
            client_email_exist ="ok"
            return render(request, 'inscription.html', locals())
            #return redirect('inscription')
        elif Client.objects.filter(telephone=request.POST['num']).exists():
             client_num_exist ="ok"
             return render(request, 'inscription.html', locals())
        else:
            clt = Client()
            clt.nom = request.POST['nom']
            clt.telephone = request.POST['num']
            clt.email = request.POST['mail']
            clt.save()

            usr = Compte()
            usr.login = request.POST['mail']
        #usr.password =  encrypt('password', request.POST['pwd'])
        usr.password =  handler.hash(request.POST['pwd'])
        
        usr.save()
        inscription_ok ="ok"
    return render(request, 'connexion.html', locals())

#mettre a jour la photo de profile
def updateProfileImg(request, id ):
    if request.method == 'POST':
        usrCmpt = Compte.objects.get(login=Client.objects.get(id=id).email)

        if usrCmpt.profile_image != request.POST['photoUploaded']: 
            usrCmpt.profile_image = request.FILES['photo']
            fs = FileSystemStorage()
            name = fs.save(usrCmpt.profile_image.name, usrCmpt.profile_image)
        usrCmpt.save()
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    return render(request, 'compte.html', locals())
#Affiche le formulaire d'ajout de vehicule
@csrf_exempt
def postForm(request, id=None):
    PaysList = Pays.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    MarqueList = Marque.objects.all()
    ModeleList = Modele.objects.all()
    TypeVoitureList = TypeVoiture.objects.all()
    optionList = Option.objects.all()
    abonnementList = Abonnement.objects.all()
    form_Voiture = VehiculeForm()
    form_Owner = OwnerForm()

    #form_OptCar = OptionVehiculeForm()
    

    freeOptionList = Option.objects.filter(type="free")
    paieOptionList = Option.objects.filter(type="paie")
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    if id != None:
        voiture = Vehicule.objects.get(id=id)
        form_Voiture = VehiculeForm(instance=voiture)
        optionListFree = freeOptionList
        optionListPaie = paieOptionList
        freeOptionVehiculeList = OptionVehicule.objects.filter(voiture=id, type="free")
        gal_array =[]
        fil_array =[]
        galCarList = GalleryVehicule.objects.filter(voiture=id)
        for  gal in galCarList:
            cities = [str(gal.fichier.read())]
            gal_array.append(str(gal.fichier.url))
            fil_array.append(str(cities))
        json_gal = json.dumps(gal_array)
        json_fil = json.dumps(fil_array)

        try: 
            proprietaire = Procuration.objects.get(id=voiture.procuration_id)
            form_Owner = OwnerForm(instance=proprietaire)
        except Procuration.DoesNotExist:
            pass
        for  listFree in freeOptionList:
            for  listFreeCar in freeOptionVehiculeList:
                if listFree.id == listFreeCar.option.id:
                    optionListFree = optionListFree.exclude(pk=listFree.id)
                else:
                    pass
        paieOptionVehiculeList = OptionVehicule.objects.filter(voiture=id, type="paie")        
        for  listPaie in paieOptionList:
            for  listPaieCar in paieOptionVehiculeList:
                if listPaie.id == listPaieCar.option.id:
                    optionListPaie = optionListPaie.exclude(pk=listPaie.id)
                else:
                    pass 
        try:
            carteGrise = DocumentVehicule.objects.get(voiture=id, type_doc="carte")
        except DocumentVehicule.DoesNotExist:
            carteGrise = DocumentVehicule()
        try:
            visite = DocumentVehicule.objects.get(voiture=id, type_doc="visite")
        except DocumentVehicule.DoesNotExist:
            visite = DocumentVehicule()    
        try:   
            assurance = DocumentVehicule.objects.get(voiture=id, type_doc="assurance")
        except DocumentVehicule.DoesNotExist:
            assurance = DocumentVehicule()
        try:   
            extincteur = DocumentVehicule.objects.get(voiture=id, type_doc="extincteur")
        except DocumentVehicule.DoesNotExist:
            extincteur = DocumentVehicule()
        return render(request, 'edit_car.html', locals())
        #return render(request, 'addTest.html', locals())

    return render(request, 'addTest.html', locals())

#JOVICE CODE AJOUT VEHICULE
def ajoutvehicule(request, id=None):
    PaysList = Pays.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    MarqueList = Marque.objects.all()
    ModeleList = Modele.objects.all()
    TypeVoitureList = TypeVoiture.objects.all()
    optionList = Option.objects.all()
    abonnementList = Abonnement.objects.all()
    form_Voiture = VehiculeForm()
    form_Owner = OwnerForm()

    # form_OptCar = OptionVehiculeForm()

    freeOptionList = Option.objects.filter(type="free")
    paieOptionList = Option.objects.filter(type="paie")
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email=cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    if id != None:
        voiture = Vehicule.objects.get(id=id)
        form_Voiture = VehiculeForm(instance=voiture)
        optionListFree = freeOptionList
        optionListPaie = paieOptionList
        freeOptionVehiculeList = OptionVehicule.objects.filter(voiture=id, type="free")
        gal_array = []
        fil_array = []
        galCarList = GalleryVehicule.objects.filter(voiture=id)
        for gal in galCarList:
            cities = [str(gal.fichier.read())]
            gal_array.append(str(gal.fichier.url))
            fil_array.append(str(cities))
        json_gal = json.dumps(gal_array)
        json_fil = json.dumps(fil_array)

        try:
            proprietaire = Procuration.objects.get(id=voiture.procuration_id)
            form_Owner = OwnerForm(instance=proprietaire)
        except Procuration.DoesNotExist:
            pass
        for listFree in freeOptionList:
            for listFreeCar in freeOptionVehiculeList:
                if listFree.id == listFreeCar.option.id:
                    optionListFree = optionListFree.exclude(pk=listFree.id)
                else:
                    pass
        paieOptionVehiculeList = OptionVehicule.objects.filter(voiture=id, type="paie")
        for listPaie in paieOptionList:
            for listPaieCar in paieOptionVehiculeList:
                if listPaie.id == listPaieCar.option.id:
                    optionListPaie = optionListPaie.exclude(pk=listPaie.id)
                else:
                    pass
        try:
            carteGrise = DocumentVehicule.objects.get(voiture=id, type_doc="carte")
        except DocumentVehicule.DoesNotExist:
            carteGrise = DocumentVehicule()
        try:
            visite = DocumentVehicule.objects.get(voiture=id, type_doc="visite")
        except DocumentVehicule.DoesNotExist:
            visite = DocumentVehicule()
        try:
            assurance = DocumentVehicule.objects.get(voiture=id, type_doc="assurance")
        except DocumentVehicule.DoesNotExist:
            assurance = DocumentVehicule()
        try:
            extincteur = DocumentVehicule.objects.get(voiture=id, type_doc="extincteur")
        except DocumentVehicule.DoesNotExist:
            extincteur = DocumentVehicule()
        return render(request, 'edit_car.html', locals())
        # return render(request, 'addTest.html', locals())

    return render(request, 'ajout-vehicule.html', locals())
   
def load_quartier(request):
    ville_id = request.GET.get('ville')
    print("ok1", ville_id)
    pivo = Quartier.objects.filter(ville_id=valnum(ville_id)).order_by('libelle')
    return render(request, 'pivo_dropdown_list_options.html', {'pivo': pivo})

def load_modele(request):
    marque_id = str(request.GET.get('marque'))
    modele = Modele.objects.filter(marque_id=valnum(marque_id)).order_by('libelle')
    vue = '< option value = "" > --------- < / option >'
    for lmodele in modele:
        vue=vue+'<option value="'+str(lmodele.id)+'">'+lmodele.libelle+'</option>'

    return HttpResponse(vue)

def load_type(request):
    modele_id = "".join(str(request.GET.get('modele')))
    vue = '< option value = "" > --------- < / option >'
    try:
        type_car = TypeVoiture.objects.filter(modele_id=valnum(modele_id)).order_by('libelle')
        for ltype_car in type_car:
            vue = vue + '<option value="' + str(ltype_car.id) + '">' + ltype_car.libelle + '</option>'

    except(ValueError, TypeError):
        type_car = TypeVoiture.objects.none()
    return HttpResponse(vue)

def addOptCar(request, type, liste, car):
    optCarList = OptionVehicule.objects.filter(voiture_id=car.id)
    if len(liste) !=0:
        optNewList = liste
        #print(len(optNewList))
        for c in liste[:] :
            
            opt = Option.objects.get(libelle=c, type=type)
            for opCar in optCarList:
                if opt == opCar.option:
                    newOpt = opCar
                    if type =="paie":
                        price = "".join(str(request.POST[c]).split())
                        print(price)
                        newOpt.prix = int(price)
                    newOpt.save()
                    liste.remove(c)
                    continue
                else:
                    continue
        #print(len(optNewList))
        for optionLibelle in liste : 
            opt = Option.objects.get(libelle=optionLibelle, type=type)
            newOpt = OptionVehicule()
            newOpt.voiture = car
            newOpt.option = opt
            newOpt.type = type
            if type =="paie":
                price = "".join(str(request.POST[optionLibelle]).split())
                print(price)
                newOpt.prix = int(price)
            newOpt.save()

def saveDocCar(request: object, type: object, docName: object, car: object, date_debut: object = timezone.now, date_fin: object = timezone.now, ref: object = None) -> object:
    try:
        instanceDoc = DocumentVehicule.objects.get(voiture_id=car.id, type_doc=type)
        docForm= DocumentVehiculeForm(request.POST, request.FILES, instance=instanceDoc)
    except DocumentVehicule.DoesNotExist:
        docForm= DocumentVehiculeForm(request.POST, request.FILES)
    if docForm.is_valid():
        doc = docForm.save(commit=False)
        try:
            doc.doc = request.FILES[docName]
        except MultiValueDictKeyError:
            pass
        if date_debut != "jj/mm/aaa" and date_debut != "":
            doc.date_debut= date_debut
        if date_fin != "jj/mm/aaa" and date_fin != "":
            doc.date_fin= date_fin
        doc.reference= ref
        doc.type_doc = type
        doc.voiture = car
        doc.save()

#test d'ajout  d'une voiture en base via django form model
#JOVICE AJOUT VEHICULE
def valideajoutvehicule(request):
    ajout = '0'
    if request.method == 'POST':
        form = VehiculeForm(request.POST, request.FILES)
        # files = request.FILES.getlist('file')
        # print(form)
        owner = Procuration()
        formProc = OwnerForm(request.POST, request.FILES)
        docForm = DocumentVehiculeForm(request.POST, request.FILES)
        if request.POST['owner'] == "non":
            if formProc.is_valid():
                model_instance = formProc.save(commit=False)
                try:
                    model_instance.fichier = request.FILES['fichier']
                except MultiValueDictKeyError:
                    pass
                model_instance.save()
                owner = model_instance

        if form.is_valid():
            car = form.save(commit=False)
            try:
                car.feature = request.FILES['feature']
            except MultiValueDictKeyError:
                pass
            # car.feature =form.cleaned_data.get('feature')
            alert = 1
            cmpt = Compte.objects.get(id=request.session.get('user_id'))
            car.client = Client.objects.get(email=cmpt.login)
            car.proprietaire = request.POST['owner']
            if request.POST['owner'] == "non":
                car.procuration = owner
            car.save()
            dz_files = [request.FILES.get('file_upload[%d]' % i) for i in range(0, len(request.FILES))]
            nbrImg = int(request.POST['car_number_img']) + 1
            print("ok1IMG", nbrImg)
            for i in range(1, nbrImg):
                try:
                    gallForm = GalleryForm(request.POST, request.FILES)
                    gallery = gallForm.save(commit=False)
                    gallery.fichier = request.FILES['user_image' + str(i)]
                    gallery.voiture = car
                    gallery.save()
                except MultiValueDictKeyError:
                    pass
            alert = 1
            context = {
                "form": form,
                "alert": alert,
            }
            freeListe = request.POST.getlist('opt_free')
            paieListe = request.POST.getlist('opt_paie')
            addOptCar(request, "free", freeListe, car)
            addOptCar(request, "paie", paieListe, car)

            saveDocCar(request, "carte", "carGray", car, request.POST['grayDAdd'], None, request.POST['grayNum'])
            saveDocCar(request, "assurance", "assure", car, request.POST['assureDStart'], request.POST['assureDEnd'])
            saveDocCar(request, "visite", "carTech", car, request.POST['techDStart'], request.POST['techDEnd'])
            saveDocCar(request, "extincteur", "carExt", car, request.POST['extDStart'], request.POST['extDEnd'])
            ajout = '1'
        else:
            print(form.errors)
            ajout = form.errors
    return  HttpResponse(ajout)

def testJs(request):
    if request.method == 'POST':
        form = VehiculeForm(request.POST, request.FILES)
        #files = request.FILES.getlist('file')
        #print(form)
        owner = Procuration()
        formProc = OwnerForm(request.POST, request.FILES)
        docForm= DocumentVehiculeForm(request.POST, request.FILES)
        if request.POST['owner'] == "non":
            if formProc.is_valid():
                model_instance = formProc.save(commit=False)
                try:
                    model_instance.fichier = request.FILES['fichier']
                except MultiValueDictKeyError:
                    pass
                model_instance.save()
                owner = model_instance
        
        if form.is_valid():
            car = form.save(commit=False)
            try:
                car.feature = request.FILES['feature']
            except MultiValueDictKeyError:
                pass
            #car.feature =form.cleaned_data.get('feature')
            alert = 1
            cmpt = Compte.objects.get(id=request.session.get('user_id')) 
            car.client = Client.objects.get(email= cmpt.login)
            car.proprietaire = request.POST['owner']
            if request.POST['owner'] == "non":
                car.procuration = owner
            """if request.POST['date_debut_location']  != "jj/mm/aaa" and request.POST['date_debut_location']  != "":
                car.date_debut_location = request.POST['date_debut_location'] 
            if  request.POST['date_fin_location'] != "jj/mm/aaa" and request.POST['date_fin_location']  != "":
               car.date_fin_location = request.POST['date_fin_location']"""
            car.save()

            dz_files = [request.FILES.get('file_upload[%d]' % i)for i in range(0, len(request.FILES))]
            nbrImg = int(request.POST['car_number_img'])+1
            print("ok1IMG", nbrImg)
            for i in range(1,nbrImg):
                try:
                    gallForm = GalleryForm(request.POST, request.FILES)
                    gallery = gallForm.save(commit=False)
                    gallery.fichier = request.FILES['user_image'+str(i)]
                    gallery.voiture = car
                    gallery.save()
                except MultiValueDictKeyError:
                    pass
            alert = 1
            context = {
                "form": form,
                "alert": alert,
            }
            freeListe = request.POST.getlist('opt_free')
            paieListe = request.POST.getlist('opt_paie')
            addOptCar(request, "free", freeListe, car)
            addOptCar(request, "paie", paieListe, car)
            
            saveDocCar(request, "carte", "carGray", car, request.POST['grayDAdd'], None,request.POST['grayNum'] )
            saveDocCar(request, "assurance", "assure", car, request.POST['assureDStart'], request.POST['assureDEnd'] )
            saveDocCar(request, "visite", "carTech", car, request.POST['techDStart'], request.POST['techDEnd'] )
            saveDocCar(request, "extincteur", "carExt", car, request.POST['extDStart'], request.POST['extDEnd'] )
     
        else:
            print("BAD")
    return JsonResponse({'status': True})

#permet d'ajouter une voiture en base 
@csrf_exempt
def postAdd(request):


    if request.method == 'POST':
        car = Vehicule()
        car.description = request.POST['desc']
        if request.POST['quartier'] =='':
            car.quartier = Quartier.objects.get(id=request.POST['quartier'])
        car.marque = request.POST['marque']
        car.modele = request.POST['modele']
        car.status = "en_aprobation"
        if request.POST['type_v'] =='':
            car.type_vehicule = TypeVoiture.objects.get(id=request.POST['type_v'])
        if request.POST['abn_mode'] =='':
            car.type_abonnement = Abonnement.objects.get(id=request.POST['abn_mode'])
        car.transmission = request.POST['tranmission']
        car.carburant = request.POST['carburant']
        car.nbr_porte = request.POST['porte']
        car.couleur = request.POST['couleur']
        place = request.POST['place']
        car.immatriculation = request.POST['matricule']
        if request.POST['place'] =='':
            place =1
        car.nbr_place = place
        car.annee_mise_en_circulation = request.POST['annee']
        km = request.POST['km']
        if request.POST['km'] =='':
            km=0
        car.kilometrage = km
        car.proprietaire = request.POST['owner']
        car.chauffeur = request.POST['carChauffeur']
        prix = request.POST['prix']
        if request.POST['prix'] ==" ":
            prix=0
        prix_km = request.POST['prixKm']
        if request.POST['prixKm'] ==" ":
            prix_km=0
        car.prix = prix
        car.prixKm = prix_km
        car.mode_paie = request.POST['modPaie']
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        car.client = Client.objects.get(email= cmpt.login)
        if request.POST['locatStart'] != "jj/mm/aaa" and request.POST['locatStart'] != "":
            car.date_debut_location = request.POST['locatStart']
        if request.POST['locatEnd'] != "jj/mm/aaa" and request.POST['locatEnd'] != "":
            car.date_fin_location = request.POST['locatEnd']


        #car.kilometrage = request.POST['opt']
        car.feature = request.FILES['carImg']
        fs = FileSystemStorage()
        name = fs.save(car.feature.name, car.feature)
        #newdoc = Permis(fichier=request.FILES.get('myfile'))
        #save procuration
        if request.POST['owner'] == "non":
            proc = Procuration()
            proc.nom = request.POST['nom_pro']
            proc.prenom = request.POST['Prenom_pro']
            proc.email= request.POST['mail_pro']
            if request.POST['d_nais_pro'] != "jj/mm/aaa" and request.POST['d_nais_pro'] != "":
                proc.date_naiss = request.POST['d_nais_pro']
            proc.civilite = request.POST['civ_pro']
            proc.tel = request.POST['tel_pro']
            if request.POST['type_v'] =='':
                proc.quartier = Quartier.objects.get(id=request.POST['quartier_pro'])
            proc.fichier = request.FILES['procFile']
            fs = FileSystemStorage()
            name = fs.save(proc.fichier.name, proc.fichier)
            proc.save()
            car.procuration = proc
        
        

        car.save()
        dz_files = request.FILES.getlist("file_upload")
        for f in dz_files:
            gallery = GalleryVehicule()
            gallery.fichier = f
            fs = FileSystemStorage()
            name = fs.save(gallery.fichier .name, gallery.fichier )
            gallery.voiture = car
            gallery.save()
        for c in request.POST.getlist('opt_free'):
            opt = Option.objects.get(libelle=c)
            newOpt = OptionVehicule()
            newOpt.voiture = car
            newOpt.option = opt
            newOpt.type = 'free'
            newOpt.save()
        for c in request.POST.getlist('opt_paie'):
            opt = Option.objects.get(libelle=c)
            newOpt = OptionVehicule()
            newOpt.voiture = car
            newOpt.option = opt
            newOpt.prix = request.POST[c]
            newOpt.type = 'paie'
            newOpt.save()

            grayDoc= DocumentVehicule()
            if request.POST['grayDAdd'] != "jj/mm/aaa" and request.POST['grayDAdd'] != "":
                grayDoc.date_debut= request.POST['grayDAdd']
            grayDoc.reference= request.POST['grayNum']
            grayDoc.doc = request.FILES['carGray']
            grayDoc.type_doc = 'carte'
            fsDoc = FileSystemStorage()
            name = fsDoc.save(grayDoc.doc.name, grayDoc.doc)
            
            grayDoc.voiture = car
            grayDoc.save()

            visiteDoc= DocumentVehicule()
            if request.POST['visiteDStart'] != "jj/mm/aaa" and request.POST['visiteDStart'] != "":
                visiteDoc.date_debut= request.POST['visiteDStart']
            if request.POST['visiteDEnd'] != "jj/mm/aaa" and request.POST['visiteDEnd'] != "":
                visiteDoc.date_fin= request.POST['visiteDEnd']
            visiteDoc.doc = request.FILES['carVis']
            visiteDoc.type_doc = 'assurance'
            fsDoc = FileSystemStorage()
            name = fsDoc.save(visiteDoc.doc.name, visiteDoc.doc)
            visiteDoc.voiture = car
            visiteDoc.save()

            techDoc= DocumentVehicule()
            if request.POST['techDStart'] != "jj/mm/aaa" and request.POST['techDStart'] != "":
                techDoc.date_debut= request.POST['techDStart']
            if request.POST['techDEnd'] != "jj/mm/aaa" and request.POST['techDEnd'] != "":
                techDoc.date_fin= request.POST['techDEnd']
            techDoc.doc = request.FILES['carTech']
            techDoc.type_doc = 'visite'
            fsDoc = FileSystemStorage()
            name = fsDoc.save(techDoc.doc.name, techDoc.doc)
            techDoc.voiture = car
            techDoc.save()
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
        return redirect('voiture/'+cmpt.id)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')     
    return render(request, 'add.html', locals())

#supprimme les vehicule de la galleries
def carDeleteGal(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email= cmpt.login)
        id = request.POST.get('idCar')
        CarFileRemove = request.POST.get('filenamenew')
        print(id)
        print(CarFileRemove)
        galList=GalleryVehicule.objects.filter(voiture_id=id)
        for fileCar in galList:
            if fileCar.fichier =="gallery/"+ CarFileRemove:
                fileCar.delete()
                os.remove("media/" + str(fileCar.fichier))   

        return JsonResponse({'status': True})
        #return redirect('affiche_compte')
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')


#permet de mettre a jour les infomation d'une voiture en base 
@csrf_exempt
def updateAdd(request):
    #print("before"+request.POST['id_car'])
    modif = '0'
    if request.method == 'POST':
            voiture = Vehicule.objects.get(id=int(request.POST['id_car']))
            form = VehiculeForm(request.POST, request.FILES, instance=voiture)
            #files = request.FILES.getlist('file')
            #print(form)
            #print("entree")
            if request.POST['id_proc'] != "":
                owner = Procuration(id=request.POST['id_proc'])
            else:
                owner = Procuration()
            formProc = OwnerForm(request.POST, request.FILES, instance=owner)
            docForm= DocumentVehiculeForm(request.POST, request.FILES)
            if request.POST['owner'] == "non":
                
                if formProc.is_valid():
                    model_instance = formProc.save(commit=False)
                    try:
                        model_instance.fichier = request.FILES['fichier']
                    except MultiValueDictKeyError:
                        pass
                    model_instance.save()
                    owner = model_instance
                else:
                    print("BADONWER")
            
            if form.is_valid():
                car = form.save(commit=False)
                try:
                    car.feature = request.FILES['feature']
                except MultiValueDictKeyError:
                    pass
                #car.feature =form.cleaned_data.get('feature')
                alert = 1
                cmpt = Compte.objects.get(id=request.session.get('user_id')) 
                car.client = Client.objects.get(email= cmpt.login)
                car.proprietaire = request.POST['owner']
                if request.POST['owner'] == "non":
                    car.procuration = owner
                """if request.POST['date_debut_location']  != "jj/mm/aaa" and request.POST['date_debut_location']  != "":
                    car.date_debut_location = request.POST['date_debut_location'] 
                if  request.POST['date_fin_location'] != "jj/mm/aaa" and request.POST['date_fin_location']  != "":
                car.date_fin_location = request.POST['date_fin_location']"""
                car.save()

                dz_files = [request.FILES.get('file_upload[%d]' % i)for i in range(0, len(request.FILES))]
                nbrImg = int(request.POST['car_number_img'])+1
                #print("ok1IMG", nbrImg)
                for i in range(1,nbrImg):
                    try:
                        gallForm = GalleryForm(request.POST, request.FILES)
                        gallery = gallForm.save(commit=False)
                        gallery.fichier = request.FILES['user_image'+str(i)]
                        gallery.voiture = car
                        gallery.save()
                    except MultiValueDictKeyError:
                        pass
                alert = 1
                context = {
                    "form": form,
                    "alert": alert,
                }
                freeListe = request.POST.getlist('opt_free')
                print("free"+ str(len(freeListe)))
                paieListe = request.POST.getlist('opt_paie')
                print("paie"+ str(len(paieListe)))
                addOptCar(request, "free", freeListe, car)
                addOptCar(request, "paie", paieListe, car)
                saveDocCar(request, "carte", "carGray", car, request.POST['grayDAdd'], None,request.POST['grayNum'] )
                saveDocCar(request, "assurance", "assure", car, request.POST['assureDStart'], request.POST['assureDEnd'] )
                saveDocCar(request, "visite", "carTech", car, request.POST['techDStart'], request.POST['techDEnd'] )
                saveDocCar(request, "extincteur", "carExt", car, request.POST['extDStart'], request.POST['extDEnd'] )
                modif = '1'
            else:
                print("BAD")
    return HttpResponse(modif)

#Affiche la liste des véhicule
def carList(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
       # return redirect('connexion')
    vehiculeList = Vehicule.objects.all().order_by('-created_at')
    form_Voiture = VehiculeForm()
    nombre= vehiculeList.count()
    PaysList = Pays.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    MarqueList = Marque.objects.all()
    ModeleList = Modele.objects.all()
    TypeVoitureList = TypeVoiture.objects.all()
    optionList = Option.objects.all()
    favorisList = Favoris.objects.filter(client_id=client.id)
    paginator = Paginator(vehiculeList, 6)
    page = request.GET.get('page')
    YEAR_CHOICES = []
    for r in range(2005, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r))
    try:
        voitures = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        voitures = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        voitures = paginator.page(paginator.num_pages)
    return render(request, 'listing-cars.html', locals())

#supression d'un vehicule en base
def delCar(request, id=None):
    car = Vehicule.objects.get(id=id)
    image = "media/" + str(car.feature)
    imageexiste = FichierExiste(image)
    if imageexiste == True:
        os.remove("media/" + str(car.feature))
    if car.delete():
        return JsonResponse({'status': True})
    else:
        return JsonResponse({'status': False})

def delCarold(request, id=None):
    try:
        car = Vehicule.objects.get(id=id)
        os.remove("media/" + str(car.feature))
        car.delete()
    except Vehicule.DoesNotExist:
        pass
    return render(request, 'mycars.html', locals())

#Affiche le formulaire de contact
def contact(request):
    
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        #return redirect('connexion')
        
    return render(request, 'contact.html', locals())

#Affiche le blog
def blog(request):
    
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()

    articleList= Article.objects.all()   
    return render(request, 'blog.html', locals())
#Affiche a propos de ybcars
def about(request):
    
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    
    return render(request, 'about.html', locals())

#Affiche a propos de ybcars
def faqs(request):
    
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    
    return render(request, 'faqs.html', locals())

#Affiche Les voitures du user connecte
def myCars(request, id):
    complet = 0
    cmpt= Compte.objects.get(id=id)
    clt = Client.objects.get(email=cmpt.login)
    Testprof = TestProfil(clt)
    vide = False
    try:
        cartList = Vehicule.objects.filter(client=clt.id)
        for p in cartList:
            complet = complet+TestVehicule(p)
        nombre= cartList.count()
    except Vehicule.DoesNotExist:
        vide = True
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    ctx = statUser(request)
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['cartList'] = cartList
    ctx['nombre'] = nombre
    ctx['vide'] = vide
    ctx['complet'] = complet
    ctx['Testprof'] = int(Testprof)
    return render(request, 'mycars.html', ctx)

#Affiche les location du user connecte
def myRent(request, id):
    cmpt= Compte.objects.get(id=id)
    client = Client.objects.get(email=cmpt.login)
    rentPaieList = []
    vide = False
    try:
        rentList = Location.objects.filter(client=client)
    except Location.DoesNotExist:
        vide = True
    
    try:
        location = Location.objects.all()
        for rental in location:
            if rental.voiture.client.id == client.id :
                rentPaieList.append(rental)
            else:
                videPaie = True
    except Location.DoesNotExist:
        vide = True
    time = datetime.datetime.now()
    ctx = statUser(request)
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['rentPaieList'] = rentPaieList
    ctx['rentList'] = rentList
    ctx['vide'] = vide
    ctx['time'] = time
    return render(request, 'myrent.html', ctx)

#Affiche les favoris du user connecte
def myLike(request, id):
    cmpt= Compte.objects.get(id=id)
    client = Client.objects.get(email=cmpt.login)
    vide = False
    try:
        favorisList = Favoris.objects.filter(client=client.id)
        nombre= favorisList.count()
    except Favoris.DoesNotExist:
        vide = True
    ctx = statUser(request)
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['favorisList'] = favorisList
    ctx['nombre'] = nombre
    ctx['vide'] = vide   
    return render(request, 'myLike.html', ctx)

#ajout au favoris  du user connecte
def like(request, id):
    cmpt= Compte.objects.get(id=request.session.get('user_id'))
    client = Client.objects.get(email=cmpt.login)
    vide = False
    existe = False
    if Favoris.objects.filter(voiture_id=id, client_id=client.id).exists():
        existe = True
    else:
        favorite = Favoris()
        favorite.client = client
        favorite.voiture = Vehicule.objects.get(id=id)
        favorite.save()
        #return redirect('recherche')
    try:
        favorisList = Favoris.objects.filter(client=client.id)
        nombre= favorisList.count()
    except Favoris.DoesNotExist:
        vide = True
    ctx = statUser(request)
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['favorisList'] = favorisList
    ctx['nombre'] = nombre
    ctx['vide'] = vide   
    return render(request, 'myLike.html', ctx)


#ajout au favoris  du user connecte
def unLike(request, id):
    cmpt= Compte.objects.get(id=request.session.get('user_id'))
    client = Client.objects.get(email=cmpt.login)
    vide = False
    existe = False
    if Favoris.objects.filter(voiture_id=id, client_id=client.id).exists():
        existe = False
        Favoris.objects.filter(voiture_id=id, client_id=client.id).delete()
        return redirect('recherche')
    else:
        pass
        fav.client = client
    try:
        favorisList = Favoris.objects.filter(client=client.id)
        nombre= favorisList.count()
    except Vehicule.DoesNotExist:
        vide = True
    ctx = statUser(request)
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['favorisList'] = favorisList
    ctx['nombre'] = nombre
    ctx['vide'] = vide   
    return render(request, 'myLike.html', ctx)

#Affiche les commentaire du user connecte
def myComment(request, id):
    cmpt= Compte.objects.get(id=id)
    client = Client.objects.get(email=cmpt.login)
    vide = False
    try:
        favorisList = Favoris.objects.filter(client=client.id)
        nombre= favorisList.count()
    except Vehicule.DoesNotExist:
        vide = True
    allComment = Commentaire.objects.all()
    UserCarList = Vehicule.objects.filter(client_id=client.id)
    userListComment = []
    for cmt in  allComment:
        for car in UserCarList:
            if cmt.voiture_id == car.id:
                userListComment.append(cmt)
    ctx = statUser(request)
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['favorisList'] = favorisList
    ctx['userListComment'] = userListComment
    ctx['nombre'] = nombre
    ctx['vide'] = vide   
    return render(request, 'comment.html', locals())
    
#Affiche les info du compte du user connecte
def userCompte(request, id):
    cmpt= Compte.objects.get(id=id)
    client = Client.objects.get(email=cmpt.login)
    testprofil = TestProfil(client)
    vide = False
    try:
        favorisList = Favoris.objects.filter(client=client.id)
        nombre= favorisList.count()
    except Vehicule.DoesNotExist:
        vide = True
    ctx = statUser(request)
    ctx['cmpt'] = cmpt
    ctx['client'] = client
    ctx['favorisList'] = favorisList
    ctx['nombre'] = nombre
    ctx['vide'] = vide
    ctx['testprofil'] = testprofil
    return render(request, 'myUserCompte.html', ctx)
#Affiche la page de validation de compte par email
def validationCompte(request, id):
    cmpt= Compte.objects.get(id=id)
    client = Client.objects.get(email=cmpt.login)
    return render(request, 'validationCompte.html', locals())
#Deconnexion  du user connecte
def logout(request, id):
    del request.session['user_id']
    return redirect('accueil')

#Deconnexion  du user connecte
def carDetail(request, id):
    car_selected = Vehicule.objects.get(id=id)
    property = Client.objects.get(id=car_selected.client.id)
    imgList = GalleryVehicule.objects.filter(voiture=id)
    owner = Compte.objects.get(login=property.email)
    freeOptionList = OptionVehicule.objects.filter(voiture=id, type="free")
    paieOptionList = OptionVehicule.objects.filter(voiture=id, type="paie")
    nombre = imgList.count()
    relatedCardList = Vehicule.objects.filter(Q(modele=car_selected.modele) | Q(prix=car_selected.prix))
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        #return redirect('connexion')
    return render(request, 'carDetail.html', locals())

def get_number_of_days(date_from, date_to):
    date_format = "%Y-%m-%d"
    a = datetime.datetime.strptime(str(date_from), date_format)
    b = datetime.datetime.strptime(str(date_to), date_format)
    delta = b - a
    return delta.days

#permet de save temporairement la location 
def showRent(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    if request.method == 'POST':
        rent = Location()
        car_selected = Vehicule.objects.get(id=request.POST['cleVoiture'])
        rent.voiture = car_selected
        rent.client = client
        rent.date_dbt = request.POST['debut']
        rent.date_fin = request.POST['fin']
        rent.statut = "tmp"
        rent.nbr_jour = get_number_of_days(rent.date_dbt, rent.date_fin)
        #rent.nbr_jour = 3
        rent.montant = car_selected.prix*rent.nbr_jour
        rent.save()
        freeOptionList = []
        paieOptionList = []
        for c in request.POST.getlist('opt'):
            print(c)
            #opt = Option.objects.get(id=c)
            optCar = OptionVehicule.objects.get(id=c)
            newOpt = OptionLocation()
            newOpt.location = rent
            newOpt.option = optCar
            newOpt.save()
            freeOptionList.append(newOpt)
        for c in request.POST.getlist('opt_paie'):
            print(c)
            #opt = Option.objects.get(id=c)
            optCar = OptionVehicule.objects.get(id=c)
            newOpt = OptionLocation()
            newOpt.location = rent
            newOpt.option = optCar
            newOpt.save()
            paieOptionList.append(newOpt)
            rent.montant = rent.montant + optCar.prix*rent.nbr_jour
        imgList = GalleryVehicule.objects.filter(voiture=car_selected)

    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    return render(request, 'paiement.html', locals())

#change le sattus de la location 
def saveRent(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')

    if request.method == 'POST':
        rent = Location.objects.get(id=request.POST['cleLoc'])
        rent.statut = "A payer"
        rent.save()
        car_rent = Vehicule.objects.get(id=rent.voiture.id)
        car_rent.status = "En location"
        car_rent.save()
        return redirect('location', id=cmpt.id)
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('connexion')
    return render(request, 'listing-cars.html', locals())

#Affiche Les voitures du user connecte
def UpdatePassword(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email= cmpt.login)
        cmpt.password =  handler.hash(request.POST['pwd1'])
        cmpt.save()       
        del request.session['user_id']
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
        return redirect('accueil')	
    return render(request, 'connexion.html', locals())

def verifyMail(request):
    cmpt = Compte.objects.get(id=request.session.get('user_id'))
    client = Client.objects.get(email= cmpt.login)
    current_site = get_current_site(request)
    msg = 'Please confirm your email address to complete the registration'
    mail_subject = 'Validation de compte.'
    message = render_to_string('acc_active_email.html', {
        'user': client,
        'domain': current_site.domain,
        'uid':urlsafe_base64_encode(force_bytes(cmpt.pk)),
        'token':account_activation_token.make_token(cmpt),
    })
    to_email = cmpt.login
    email = EmailMessage(
                mail_subject, message, to=[to_email]
    )
    email.send()
    msg = 'Veuillez confirmer votre adresse e-mail pour finaliser la verification'
    return render(request, 'myUserCompte.html', locals())
#Validadition du mail avec l'id compte en parametres
def validationMail(request, id):
    semail = 0
    postdata = 1
    cmpt = Compte.objects.get(id=id)
    emailuser = cmpt.login
    client = Client.objects.get(email= cmpt.login)
    current_site = get_current_site(request)
    msg = 'Please confirm your email address to complete the registration'
    mail_subject = 'Validation de compte.'
    message = render_to_string('acc_active_email.html', {
        'user': client,
        'domain': current_site.domain,
        'uid':urlsafe_base64_encode(force_bytes(cmpt.pk)),
        'token':account_activation_token.make_token(cmpt),
    })
    to_email = cmpt.login
    email = EmailMessage(
                mail_subject, message, to=[to_email]
    )
    if email.send():
        semail = 1
    else:
        semail = 0
    msg = 'Veuillez confirmer votre adresse e-mail pour finaliser la verification'
    return render(request, 'validationCompte.html', locals())
#activation du mail
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        cmpt = Compte.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Compte.DoesNotExist):
        cmpt = None
    if cmpt is not None and account_activation_token.check_token(cmpt, token):
        cmpt.vermail = '1'
        cmpt.save()
        return redirect('compte')
        
    else:
        msg = 'Activation link is invalid!'
        return render(request, 'myUserCompte.html', locals())

def statUser(request):
    utilisateur = Compte.objects.get(id=request.session.get('user_id'))
    cli = Client.objects.get(email= utilisateur.login)
    user_gain=0
    nbre_voiture = 0
    user_pay = 0
    user_other_rent = 0
    user_myrent = 0
    try:
        nbre_voiture =  Vehicule.objects.filter(client_id=cli.id).count()
    except Vehicule.DoesNotExist:
        nbre_voiture = 0
    try:
        location = Location.objects.all()
        for rental in location:
            if rental.voiture.client.id == cli.id :
                user_gain=user_gain + rental.montant
                user_myrent = user_myrent+1
            if rental.client.id == cli.id :
                user_pay=user_pay + rental.montant
                user_other_rent = user_other_rent+1
    except Location.DoesNotExist:
        user_gain=0
    print(nbre_voiture)
    ctx = {
    'user_gain': user_gain,
    'user_pay': user_pay,
    'nbre_voiture': nbre_voiture,
    'user_other_rent': user_other_rent,
    'user_myrent': user_myrent
    }
    return ctx

def saveNotif(request):
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email= cmpt.login)
        NotifUser.objects.filter(client_id=cmpt.id).delete()
        for c in request.POST.getlist('notif'):
            notificate = Notification.objects.get(id=c)
            notif = NotifUser()
            notif.client = cmpt
            notif.notification = notificate
            notif.save()
        return redirect('compte')
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
    return render(request, 'myUserCompte.html', locals())

def carChangeStat(request):
    id = request.POST.get('car')
    st = request.POST.get('stat')
    print(id)
    print(st)
    try:
        car_selected = Vehicule.objects.get(id=id)
        car_selected.status = st
        car_selected.save()
    except Vehicule.DoesNotExist:
        pass
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email= cmpt.login)
        #return redirect('affiche_compte')
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
    return JsonResponse({'status': True})

def commentCar(request):
    id = request.POST['car']
    etat = request.POST['etat']
    cmt = request.POST['comment']
    try:
        car_selected = Vehicule.objects.get(id=id)
    except Vehicule.DoesNotExist:
        pass
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email= cmpt.login)
        comment = Commentaire()
        comment.comment = cmt
        comment.etat = etat
        comment.voiture = car_selected
        comment.client = client
        comment.save()
        return redirect('location', id=request.session.get('user_id'))
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
 
    return render(request, 'myUserCompte.html', locals())

#retour effectif de location pour remetre le vehicule en disponibilite
def retourCar(request):
    id = request.POST['local']
    date_retour = request.POST['date_retour']
    cmt = request.POST['comment']
    try:
        rent = Location.objects.get(id=id)
    except Location.DoesNotExist:
        pass
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email= cmpt.login)
        rent.comment = cmt
        rent.retour = date_retour
        rent.save()
        car_rent = Vehicule.objects.get(id=rent.voiture.id)
        car_rent.status = "Disponible"
        car_rent.save()
        return redirect('location', id=request.session.get('user_id'))
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
 
    return render(request, 'myUserCompte.html', locals())

def resultCar(request):
    critere = request.POST.get('critere')
    mrqs = request.POST.getlist('marque')
    transmis = None
    for trans in request.POST.getlist('transmission'):
        transmis= trans
    vehiculeList=[]
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        pass
        #cmpt = Compte()
        #client = Client()
        #return redirect('connexion')
    try:
        if critere:
            vehiculeList = Vehicule.objects.filter(marque__icontains=critere)
            print(critere)
            if transmis != None:
                vehiculeList = Vehicule.objects.filter(marque__icontains=critere).filter(transmission__icontains=transmis)
                print("ok2")
                if mrqs :
                    for mk in mrqs:
                        if mk == "all":
                            vehiculeList = Vehicule.objects.filter(marque__icontains=critere).filter(transmission__icontains=transmis)
                            print("ok5")
                            break
                        else:
                            tmp.append(mk)
                    if not tmp:
                        pass
                    else:
                        vehiculeList = Vehicule.objects.filter(marque__icontains=critere).filter(transmission__icontains=transmis).filter(marque__in=mrqs)    
        elif transmis:
            vehiculeList = Vehicule.objects.filter(transmission__icontains=transmis)
            print("ok4")
            tmp=[]
            if mrqs:
                for mk in mrqs:
                    if mk == "all":
                        vehiculeList = Vehicule.objects.filter(transmission__icontains=transmis)
                        print("ok5")
                        break
                    else:
                        tmp.append(mk)
                if not tmp:
                    pass
                else:
                    vehiculeList = Vehicule.objects.filter(transmission__icontains=transmis).filter(marque__in=tmp)
        elif mrqs :
            tmp=[]
            for mk in mrqs:
                if mk == "all":
                    vehiculeList = Vehicule.objects.all()
                    print("ok5")
                    break
                else:
                    tmp.append(mk)
            if not tmp:
                pass
            else:
                vehiculeList = Vehicule.objects.filter(marque__in=mrqs)      
        #vehiculeList = Vehicule.objects.filter(marque__icontains=critere).filter(transmission__icontains=transmis).filter(marque__in=mrqs)
    except ValueError:
        vehiculeList=[]
    #nombre= vehiculeList.count()
    PaysList = Pays.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    MarqueList = Marque.objects.all()
    ModeleList = Modele.objects.all()
    TypeVoitureList = TypeVoiture.objects.all()
    optionList = Option.objects.all()
    favorisList = Favoris.objects.filter(client_id=client.id)
 
    return render(request, 'resultat.html', locals())

def resultHome(request):
    qs = Vehicule.objects.all()
    if request.method == 'POST':
        mrqs = request.POST['marque']
        annee = request.POST['annee']
        ville = request.POST['ville']
        quartier = request.POST['quartier']
        modl = request.POST['modele']
        dateStart = request.POST['dateStart']
        dateEnd = request.POST['dateEnd']
        prix = request.POST['prix']
        try:
            transmission = request.POST['transmission']
        except MultiValueDictKeyError:
            transmission = ""
        #print(request.POST)
        if mrqs != '' and mrqs is not None:
            qs = qs.filter(marque=mrqs)
        if modl != '' and modl is not None:
            qs = qs.filter(modele=modl)
        if ville != '' and ville is not None:
            qs = qs.filter(ville=ville)
        if quartier != '' and quartier is not None:
            qs = qs.filter(quartier=quartier)
        if annee != '' and annee is not None:
            stranne = annee.split()
            intanne = int(stranne[0]+stranne[1])
            qs = qs.filter(annee_mise_en_circulation=intanne)
        if transmission != '' and transmission is not None:
            qs = qs.filter(transmission=transmission)
        if prix != '' and prix is not None:
            try:
                qs = qs.filter(prix=prix)
            except ValueError:
                pass
        if dateStart != "jj/mm/aaa" and dateStart!='': 
            if dateEnd != "jj/mm/aaa" and dateEnd!='':
            #qs = qs.filter(date_debut_location__range=(dateStart, dateEnd))
                qs = qs.filter( Q(date_debut_location__gte=dateStart, date_debut_location__lt=dateEnd))
            else:
                qs = qs.filter( Q(date_debut_location__gte=dateStart))
        elif dateEnd != "jj/mm/aaa" and dateEnd!='':
            qs = qs.filter( Q(date_debut_location__lt=dateEnd))
        else:
            pass


        #qs = qs.filter(date_debut_location__lte=dateEnd, date_debut_location__gt=dateStart)
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id')) 
        client = Client.objects.get(email= cmpt.login)
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
    vehiculeList= qs
    form_Voiture = VehiculeForm()
    nombre= vehiculeList.count()
    PaysList = Pays.objects.all()
    VilleList = Ville.objects.all()
    QuartierList = Quartier.objects.all()
    MarqueList = Marque.objects.all()
    ModeleList = Modele.objects.all()
    TypeVoitureList = TypeVoiture.objects.all()
    optionList = Option.objects.all()
    favorisList = Favoris.objects.filter(client_id=client.id)
    paginator = Paginator(vehiculeList, 6)
    page = request.GET.get('page')
    YEAR_CHOICES = []
    for r in range(2005, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r))
    try:
        voitures = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        voitures = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        voitures = paginator.page(paginator.num_pages)
 
    return render(request, 'resultat.html', locals())


def postDetail(request, id):

    try:
        article = Article.objects.get(id=id)
    except Article.DoesNotExist:
        pass
    try:
        cmpt = Compte.objects.get(id=request.session.get('user_id'))
        client = Client.objects.get(email= cmpt.login)
        articleList = Article.objects.all()
        #return redirect('location', id=request.session.get('user_id'))
    except Compte.DoesNotExist:
        cmpt = Compte()
        client = Client()
 
    return render(request, 'blog-details.html', locals())

def SendMail(request):
    cmpt = Compte.objects.get(id=request.session.get('user_id'))
    client = Client.objects.get(email= cmpt.login)
    current_site = get_current_site(request)
    sender = Message()
    sender.nom = request.POST['name']
    sender.message = request.POST['message']
    sender.sujet = request.POST['subject']
    sender.email = request.POST['email']
    sender.save()
    to_email = "nene.rostin21@gmail.com"
    email = EmailMessage(
                sender.sujet, sender.message , to=[to_email]
    )
    email.send()
    msg = 'Votre mail est ok'
    return render(request, 'contact.html', locals())




# =============================   JOVICE_FONCTIONS PYTHON
def TestProfil(client):
    #print(client)
    retour = 0
    retour = retour+int(TestValeur(client.id))
    retour = retour + int(TestValeur(client.nom))
    #retour = retour + int(TestValeur(client.date_naissance))
    retour = retour + int(TestValeur(client.telephone))
    retour = retour + int(TestValeur(client.statut))
    retour = retour + int(TestValeur(client.titre))
    retour = retour + int(TestValeur(client.permis_id))
    retour = retour + int(TestValeur(client.quartier_id))
    return retour
def TestValeur(champ):
    if len(str(champ)) > 0:
        return 0
    else:
        return 1

def TestImmat(request):
    immat = str(request.GET.get('immat'))
    try:
        doc = Vehicule.objects.get(immatriculation=immat)
    except Vehicule.DoesNotExist:
        return HttpResponse(0)
    return HttpResponse(doc.id)

def TestVehicule(Vehicule):
    retour = 0
    retour = retour + int(TestValeur(Vehicule.prix))
    retour = retour + int(TestValeur(Vehicule.mode_paie))
    retour = retour + int(TestValeur(Vehicule.type_abonnement_id))
    try:
        car = DocumentVehicule.objects.filter(voiture=Vehicule.id)
        for v in car:
            retour = retour + int(TestValeur(v.doc))
            retour = retour + int(TestValeur(v.date_debut))
            retour = retour + int(TestValeur(v.date_fin))
    except DocumentVehicule.DoesNotExist:
        retour = 1
    return retour


def valnum(an):
    stranne = an.split()
    sortir = ''
    for p in stranne:
        sortir = sortir + p
    return int(sortir)