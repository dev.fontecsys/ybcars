function NotifJS(code){
    if(code == 1){
        document.getElementById("alertnotif").innerHTML= '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>Opération effectuée avec succes.</div>';
    }else{
        document.getElementById("alertnotif").innerHTML= '<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong> Une erreur s\'est produite penadant le traitement</div>';
    }
}
function ResetNotifJS(){
    document.getElementById("alertnotif").innerHTML= '';
}
function ChangeValue(id,value){
    document.getElementById(id).value = value;
}
function FormatageDate(elm){
    LongeurTexte = elm.value.length;
    if(LongeurTexte == 2){
      if(elm.value <= 31){
        elm.value = elm.value+"/";
        remettreNormalement(elm);
      }else{
        changerCouleur(elm);
        elm.value = ""  
      }
    }else if(LongeurTexte == 5){
      if(elm.value.substring(3, 5) <= 12){
        elm.value = elm.value+"/";
        remettreNormalement(elm);
      }else{
        changerCouleur(elm);
        elm.value = elm.value.substring(0, 3); 
      }
    }else if(LongeurTexte > 10){
      elm.value = elm.value.substring(0, 10);
    }
}
function ValideFormulaire(element){
    //element = "recherche";
    if(document.getElementById(element).value == ""){
        document.getElementById(element).style.color  = "black";
        document.getElementById(element).style.backgroundColor  = "red";
        return false;
    }else{
        return true;
    }
}
function DefailtStyleCSS(element){
        document.getElementById(element.id).style.color  = "black";
        document.getElementById(element.id).style.backgroundColor  = "white";
}
function valListe(IDELEMENR){
    var retour
    select = document.getElementById(IDELEMENR);
    choice = select.selectedIndex;  // Récupération de l'index du <option> choisi
    retour = select.options[choice].value;
    return retour;
}
function TestChamp(IDELEMENR){
	valeur = document.getElementById(IDELEMENR).value;
	erreur = 0;
	if(valeur == '' | valeur == 'none' ){
		erreur = 1;
	}
	return erreur;
}
function Desactiverchamp(idElement){
    document.getElementById(idElement).disabled = true;
}
function Activerchamp(idElement){
    document.getElementById(idElement).disabled = false;
}
function CaElement(idElement){
    document.getElementById(idElement).style.display='none'; 
}
function Afficherdiv(idElement){
    document.getElementById(idElement).style.display='block'; 
}
function autoclick(idElement){
    document.getElementById(idElement).click();
}
function test(){
    alert("Fichier js visible");
}
function FiltreTableaux(inputtext,tableauid,col1,col2,col3) {
        var input, filter, table, tr, td, i;
        input  = document.getElementById(inputtext);
        filter = input.value.toUpperCase();
        table  = document.getElementById(tableauid);
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[col1];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
                  tr[i].style.display = "none";
                  td = tr[i].getElementsByTagName("td")[col2];
                  if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                   } else {
                    tr[i].style.display = "none";
                        td = tr[i].getElementsByTagName("td")[col3];
                        if (td) {
                          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                          tr[i].style.display = "";
                         } else {
                          tr[i].style.display = "none";
                          }
                        }
                    }
                  }
            }
          }
        }
}
function validevaleur(idchamp,code){
    var sortie = []
    var x = document.getElementById(idchamp).value;
    //var msg = document.getElementById(idchamp).innerHTML ;
    switch (code) {
        case 1:
        case 2:
            if (isNaN(x)){
                sortie[2] ="Entrez une valeur numérique ";
                sortie[1] =  false;
            }else{
                sortie[1] =  "";
                sortie[2] =  true;
            }
        case 3:
            var atposition=x.indexOf("@");
            var dotposition=x.lastIndexOf(".");
            if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){
                sortie[2] = "Email non valide";
                sortie[1] =  false;
            }
    }
}
