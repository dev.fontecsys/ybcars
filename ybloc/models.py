# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
import os
from uuid import uuid4
from django.utils import timezone
from django.conf import settings
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator


def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)

def path_and_rename(instance, filename):
    instance = Vehicule.objects.last()
    path = 'voiture/{}/doc/'
    path.format
    upload_to = path.format(instance.id)
    ext = filename.split('.')[-1]
    # get filename
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)

def path_and_rename_client(instance, filename):
    instance = Vehicule.objects.last()
    path = 'client/{}/doc/'
    path.format
    upload_to = path.format(instance.id)
    ext = filename.split('.')[-1]
    # get filename
    if instance.pk:
        filename = '{}.{}'.format(instance.pk, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)

class Pays(models.Model):
    libelle = models.CharField(unique=True, max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    class Meta:
        verbose_name_plural = "Pays"

class Ville(models.Model):
    libelle = models.CharField(unique=True, max_length=191, blank=True, null=True)
    pays = models.ForeignKey(Pays, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle

class Quartier(models.Model):
    libelle = models.CharField( max_length=191, blank=True, null=True)
    ville = models.ForeignKey(Ville, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle


class Permis(models.Model):
    TYPE_PERMIS = (
        ("A","A"),
        ("B","B"),
        ("C","C"),
        ("D","D"),
    )
    numero = models.CharField(max_length=191, blank=True, null=True)
    fichier = models.FileField(upload_to='permis/', blank=True, null=True)
    date_emission = models.DateField(blank=True, null=True)
    type = models.CharField('Type', max_length=100, choices=TYPE_PERMIS)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    def  __unicode__(self):
        return self.numero
    class Meta:
        verbose_name_plural = "Permis"

class Compte(models.Model):
    login = models.CharField(max_length=30, blank=True, null=True)
    password = models.TextField()
    vernum = models.CharField(max_length=30, blank=True, null=True)
    vermail = models.CharField(max_length=30, blank=True, null=True)
    verdoc = models.CharField(max_length=30, blank=True, null=True)
    last = models.DateTimeField(blank=True, null=True)
    profile_image = models.ImageField(upload_to='images/users/', blank=True, null=True, default='..{}images/default_avatar.png'.format(settings.STATIC_URL))

    def  __unicode__(self):
        return self.login


class Client(models.Model):
    TYPE_CLIENT = (
        ("P","Propretaire"),
        ("L","Locataire"),
    )
    TITRE = (
        ("M","Monsieur"),
        ("MLLE","Mademoisele"),
        ("MME","Madame"),
    )

    STATUT = (
        ("Etudiant","Etudiant"),
        ("Salarié","Salarié"),
    )   
    nom = models.CharField(max_length=50, blank=True, null=True)
    prenom = models.CharField(max_length=50, blank=True, null=True)
    date_naissance = models.DateField(blank=True, null=True)
    compte = models.ForeignKey(Compte, blank=True, null=True, on_delete=models.CASCADE)
    telephone = models.CharField(max_length=30, blank=True, null=True)
    email = models.CharField(unique=True, max_length=191, blank=True, null=True)
    adresse = models.CharField(max_length=100, blank=True, null=True)
    bp = models.CharField(max_length=100, blank=True, null=True)
    quartier = models.ForeignKey(Quartier, blank=True, null=True, on_delete=models.CASCADE)
    permis = models.ForeignKey(Permis, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    statut = models.CharField('statut salarial', max_length=100, choices=STATUT, blank=True, null=True)
    titre = models.CharField('Titre', max_length=100, choices=TITRE, blank=True, null=True)
    doc = models.FileField(upload_to=path_and_rename_client, blank=True, null=True)
    entite = models.CharField('Entité', max_length=100, blank=True, null=True)
    type = models.CharField('type client',  max_length=1, choices=TYPE_CLIENT, blank=True, null=True)

    def  __unicode__(self):
        return self.nom


class Marque(models.Model):
    libelle = models.CharField(unique=True, max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle

class Abonnement(models.Model):
    mode = models.CharField(unique=True, max_length=191, blank=True, null=True)
    prix = models.FloatField( blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __str__ (self):
        return self.mode

class Modele(models.Model):
    libelle = models.CharField( max_length=191, blank=True, null=True)
    marque = models.ForeignKey(Marque, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle

class TypeVoiture(models.Model):
    libelle = models.CharField( max_length=191, blank=True, null=True)
    modele = models.ForeignKey(Modele, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle

class Option(models.Model):
    TYPE = (
        ("paie","paie"),
        ("free","free"),
    )
    libelle = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    type = models.CharField('type option', max_length=100, choices=TYPE, blank=True, null=True)

    
    def __str__ (self):
        return  self.libelle


class Transmission(models.Model):
    libelle = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle

class Usage(models.Model):
    libelle = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle

class Couleur(models.Model):
    libelle = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle



class Place(models.Model):
    libelle = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle
class Porte(models.Model):
    libelle = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        return  self.libelle

class Procuration(models.Model):
    TITRE = (
        ("M","Monsieur"),
        ("MLLE","Mademoisele"),
        ("MME","Madame"),
    )
    civilite = models.CharField('Titre', max_length=100, choices=TITRE, blank=True, null=True)
    nom = models.CharField( max_length=191, blank=True, null=True)
    prenom = models.CharField(max_length=191, blank=True, null=True)
    email = models.CharField( max_length=191, blank=True, null=True)
    tel = models.CharField( max_length=191, blank=True, null=True)
    date_naiss = models.DateTimeField(blank=True, null=True)
    ville = models.ForeignKey(Ville, blank=True, null=True, on_delete=models.CASCADE)
    quartier = models.ForeignKey(Quartier, blank=True, null=True, on_delete=models.CASCADE)
    fichier = models.FileField(upload_to='procuration/', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def __str__ (self):
        if self.nom == None:
            return  ""
        else:
            return  self.nom

class Vehicule(models.Model):
    YEAR_CHOICES = []
    for r in range(2005, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r,r))

    CARBURANT = (
        ("Essence","Essence"),
        ("Diesel","Diesel"),
        ("Hybride","Hybride"),
        ("Électrique","Électrique"),
        ("None","Aucun"),
    )
    CHOIX = (
        ("Oui","Oui"),
        ("Non","Non"),
        ("None","Aucun"),

    )    
    TRANSMISSION = (
        ("Automatique","Automatique"),
        ("Manuelle","Manuelle"),
        ("None","Aucun"),
    )  
    MODE_PAIEMENT = (
        ("abonnement","abonnement"),
        ("location","location"),
        ("None","Aucun"),
    )
    STATUS = (
        ("Disponible","Disponible"),
        ("Indisponible","Indisponible"),
        ("Publié","Publié"),
        ("En location","En location"),
    )  
    description = models.CharField(max_length=191, blank=True, null=True, default='' )
    modele = models.ForeignKey(Modele, blank=True, null=True, on_delete=models.CASCADE, default='')
    marque = models.ForeignKey(Marque, blank=True, null=True, on_delete=models.CASCADE, default='')
    type_vehicule = models.ForeignKey(TypeVoiture, blank=True, null=True, on_delete=models.CASCADE, default='')
    type_abonnement = models.ForeignKey(Abonnement, blank=True, null=True, on_delete=models.CASCADE, default='')
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE, default='')
    nbr_place = models.ForeignKey(Place, blank=True, null=True, on_delete=models.CASCADE, default='')
    nbr_porte = models.ForeignKey(Porte, blank=True, null=True, on_delete=models.CASCADE, default='')
    couleur = models.ForeignKey(Couleur, blank=True, null=True, on_delete=models.CASCADE, default='')
    prix = models.IntegerField( blank=True, null=True, default='')
    prixKm = models.IntegerField( blank=True, null=True, default='')
    kilometrage = models.FloatField( blank=True, null=True, default='')
    immatriculation = models.CharField(unique=True, max_length=191, blank=True, null=True, default='')
    nr_chassis = models.CharField(unique=True, max_length=191, blank=True, null=True, default='')
    date_mise_en_circulation = models.DateField(blank=True, null=True)
    annee_mise_en_circulation = models.IntegerField(('année'), choices=YEAR_CHOICES, default=datetime.datetime.now().year)
    carburant = models.CharField('Carburant', max_length=100, choices=CARBURANT, blank=True, null=True, default='')
    transmission = models.CharField('transmission', max_length=100, choices=TRANSMISSION, blank=True, null=True, default='')
    proprietaire = models.CharField('proprietaire', max_length=100, choices=CHOIX, blank=True, null=True)
    chauffeur = models.CharField('chauffeur', max_length=100, choices=CHOIX, blank=True, null=True)
    mode_paie = models.CharField('Mode de paiement', max_length=100, choices=MODE_PAIEMENT, blank=True, null=True, default='')
    status = models.CharField('status', max_length=100, choices=STATUS, blank=True, null=True, default='')
    feature = models.FileField(upload_to='voiture/', blank=True, null=True, default='')
    procuration = models.ForeignKey(Procuration, blank=True, null=True, on_delete=models.CASCADE, default='')
    ville = models.ForeignKey(Ville, blank=True, null=True, on_delete=models.CASCADE, default='')
    quartier = models.ForeignKey(Quartier, blank=True, null=True, on_delete=models.CASCADE, default='')
    usage = models.ForeignKey(Usage, blank=True, null=True, on_delete=models.CASCADE, default='')

    created_at = models.DateTimeField(auto_now_add=True)
    date_debut_location = models.DateTimeField(blank=True, null=True)
    date_fin_location = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True )

    def  __unicode__(self):
        return self.description

class OptionVehicule(models.Model):
    TYPE = (
        ("paie","paie"),
        ("free","free"),
        ("None","Aucun"),
    )
    voiture = models.ForeignKey(Vehicule, blank=True, null=True, on_delete=models.CASCADE)
    option = models.ForeignKey(Option, blank=True, null=True, on_delete=models.CASCADE)
    prix = models.BigIntegerField(blank=True, null=True)
    type = models.CharField('type option', max_length=100, choices=TYPE, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.voiture

class Favoris(models.Model):
    voiture = models.ForeignKey(Vehicule, blank=True, null=True, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.voiture

class GalleryVehicule(models.Model):
    voiture = models.ForeignKey(Vehicule, blank=True, null=True, on_delete=models.CASCADE)
    fichier = models.FileField(upload_to='gallery/', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.voiture

class DocumentVehicule(models.Model):
    TYPE = (
        ("carte","carte"),
        ("assurance","assurance"),
        ("visite","visite"),
        ("extincteur","extincteur"),
    )
    voiture = models.ForeignKey(Vehicule, blank=True, null=True, on_delete=models.CASCADE)
    doc = models.FileField(upload_to=path_and_rename, blank=True, null=True)
    date_debut = models.DateTimeField(blank=True, null=True)
    date_fin = models.DateTimeField(blank=True, null=True)
    reference = models.CharField(unique=True, max_length=191, blank=True, null=True)
    type_doc = models.CharField('type document', max_length=100, choices=TYPE, blank=True, null=True)

    def  __unicode__(self):
        return self.voiture

class Location(models.Model):
    client= models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE)
    voiture = models.ForeignKey(Vehicule, blank=True, null=True, on_delete=models.CASCADE)
    avec_chauffeur = models.IntegerField(blank=True, null=True)
    client_est_chauffeur = models.IntegerField(blank=True, null=True)
    livraison_effective = models.IntegerField(blank=True, null=True)
    long_duree = models.IntegerField(blank=True, null=True)
    niveau_carburant = models.CharField(max_length=5, blank=True, null=True)
    date_dbt = models.DateTimeField(blank=True, null=True)
    date_fin = models.DateTimeField(blank=True, null=True)
    tarif_journalier = models.FloatField(blank=True, null=True)
    nbr_jour = models.FloatField(blank=True, null=True)
    montant = models.FloatField(blank=True, null=True)
    statut = models.CharField( max_length=100, blank=True, null=True)
    lavage_carburant = models.FloatField(blank=True, null=True)
    frais_livraison = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    retour = models.DateTimeField(blank=True, null=True)
    comment = models.CharField( max_length=100, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    
    def  __unicode__(self):
        return self.montant

class OptionLocation(models.Model):
    location = models.ForeignKey(Location, blank=True, null=True, on_delete=models.CASCADE)
    option = models.ForeignKey(OptionVehicule, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.location

class Notification(models.Model):
    titre = models.CharField( max_length=250, blank=True, null=True)
    message = models.CharField( max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.titre

class NotifUser(models.Model):
    notification = models.ForeignKey(Notification, blank=True, null=True, on_delete=models.CASCADE)
    client = models.ForeignKey(Compte, blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.notification

class Commentaire(models.Model):
    voiture = models.ForeignKey(Vehicule, blank=True, null=True, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE)
    comment = models.CharField( max_length=250, blank=True, null=True)
    etat = models.CharField( max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.comment

class Article(models.Model):
    auteur = models.ForeignKey('auth.User', blank=True, null=True, on_delete=models.CASCADE)
    titre = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    texte = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='blog/', blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.titre

class Message(models.Model):
    nom = models.CharField( max_length=250, blank=True, null=True)
    email = models.CharField( max_length=250, blank=True, null=True)
    sujet = models.CharField( max_length=250, blank=True, null=True)
    message = models.CharField( max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.nom

class PaiementClient(models.Model):
    MODE = (
        ("Virement bancaire","Virement bancaire"),
        ("Airtel money","Airtel money"),
        ("Visa","Visa"),
        ("Espèce / cheque","Espèce / cheque"),
    )
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE)
    mode = models.CharField('Mode', max_length=100, choices=MODE, blank=True, null=True)
    reference = models.CharField( max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    def  __unicode__(self):
        return self.mode