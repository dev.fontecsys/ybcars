from django import forms
from django.utils import formats, translation
import datetime
from django.contrib.admin.widgets import AdminDateWidget
from django.forms import widgets
from .models import *
def get_years():
    years = []
    for year in range(1980, (datetime.datetime.now().year)):
        years.append(year)
    return sorted(years, reverse=True)

FIELD_NAME_MAPPING = {
    'ville': 'onwer_ville',
    'quartier': 'onwer_quartier'
}
def valnum(an):
    stranne = an.split()
    sortir = ''
    for p in stranne:
        sortir = sortir + p
    return int(sortir)
class DateInput(forms.DateInput):
    input_type = "date"

    def __init__(self, **kwargs):
        kwargs["format"] = "%Y-%m-%d"
        super().__init__(**kwargs)

#formulaire du vehicule
class VehiculeForm(forms.ModelForm):
    class Meta:
        model = Vehicule
        exclude = ['nr_chassis', 'date_mise_en_circulation']



    def __init__(self, *args, **kwargs):
        YEAR_CHOICES = []
        for r in range(2005, (datetime.datetime.now().year+1)):
            YEAR_CHOICES.append((r,r))
        super(VehiculeForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
        #validation du quartier
        self.fields['quartier'].queryset = Quartier.objects.none()
        #self.fields['quartier'].empty_label = "Choisissez un quartier"
        self.fields['ville'].empty_label = "Choisissez une ville"
        self.fields['quartier'].empty_label = "Choisissez un quartier"
        #self.fields['quartier'].widget.attrs['data-placeholder']= 'Choose members..'
        if 'ville' in self.data:
            try:
                ville_id = valnum(self.data.get('ville'))
                self.fields['quartier'].queryset = Quartier.objects.filter(ville_id=ville_id).order_by('libelle')
                self.fields['quartier'].widget.attrs['placeholder'] = 'ville'
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['quartier'].queryset = Quartier.objects.filter(ville_id=self.instance.ville_id).order_by('libelle')

        #validation du modele de voiture
        self.fields['modele'].queryset = Modele.objects.none()
        self.fields['modele'].empty_label = "Choisissez un modèle"
        self.fields['marque'].empty_label = "Choisissez une marque"
        if 'marque' in self.data:
            try:
                marque_id = valnum(self.data.get('marque'))
                self.fields['modele'].queryset = Modele.objects.filter(marque_id=marque_id).order_by('libelle')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            if self.instance.marque == None:
                pass
            else:
                self.fields['modele'].queryset = self.instance.marque.modele_set.order_by('libelle')


        #validation du type de voiture
        self.fields['type_vehicule'].queryset = TypeVoiture.objects.none()
        self.fields['type_vehicule'].empty_label = "Choisissez un type de voiture"

        if 'modele' in self.data:
            try:
                modele_id = valnum(self.data.get('modele'))
                self.fields['type_vehicule'].queryset = TypeVoiture.objects.filter(modele_id=modele_id).order_by('libelle')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            #p = Modele.objects.get(id=self.instance.modele_id)
            self.fields['type_vehicule'].queryset = TypeVoiture.objects.filter(modele_id=self.instance.modele_id).order_by('libelle')
            #self.fields['type_vehicule'].queryset = self.instance.modele.typevehicule_set.all().order_by('libelle')
            #self.fields['type_vehicule'].queryset = TypeVoiture.objects.all()
        self.fields['annee_mise_en_circulation'] = forms.ChoiceField(choices=YEAR_CHOICES )
        self.fields['date_fin_location'].widget = DateInput()
        self.fields['date_fin_location'].widget.attrs.update({'class': 'form-control'})
        self.fields['date_debut_location'].widget = DateInput()
        self.fields['date_debut_location'].widget.attrs.update({'class': 'form-control'})
#formulaire du proprietaire
class OwnerForm(forms.ModelForm):
    def add_prefix(self, field_name):
        # look up field name; return original if not found
        field_name = FIELD_NAME_MAPPING.get(field_name, field_name)
        return super(OwnerForm, self).add_prefix(field_name)
    class Meta:
        model = Procuration
        fields = '__all__' 
    def __init__(self, *args, **kwargs):
        super(OwnerForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            #self.fields['ville'].widget.attrs.update({'id': 'onwer_ville_id'})


#formulaire de gallerie du vehicule
class GalleryForm(forms.ModelForm):

    class Meta:
        model = GalleryVehicule
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(GalleryForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

#formulaire de gallerie du vehicule
class OptionVehiculeForm(forms.ModelForm):

    class Meta:
        model = OptionVehicule
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(OptionVehicule, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

#formulaire de gallerie du vehicule
class DocumentVehiculeForm(forms.ModelForm):

    class Meta:
        model = DocumentVehicule
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DocumentVehiculeForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

#formulaire du vehicule
class clientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'


    def __init__(self, *args, **kwargs):

        super(clientForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
        #validation du quartier
        self.fields['quartier'].queryset = Quartier.objects.none()
        if 'ville' in self.data:
            try:
                ville_id = int(self.data.get('ville'))
                self.fields['quartier'].queryset = Quartier.objects.filter(ville_id=ville_id).order_by('libelle')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['quartier'].queryset = self.instance.ville.quartier_set.order_by('libelle')