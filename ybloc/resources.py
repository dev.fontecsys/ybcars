from import_export import resources
from .models import Marque, Modele, TypeVoiture, Quartier, Option

class MarqueResource(resources.ModelResource):
    class Meta:
        model = Marque

class ModeleResource(resources.ModelResource):
    class Meta:
        model = Modele

class TypeVoitureResource(resources.ModelResource):
    class Meta:
        model = TypeVoiture

class QuartierResource(resources.ModelResource):
    class Meta:
        model = Quartier

class OptionResource(resources.ModelResource):
    class Meta:
        model = Option